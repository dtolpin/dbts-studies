"""Unit tests for predict.
"""

import numpy
import pytest

from intensix.monitor.predict import *


def test_sync_preds():
    """Tests that sync_pred rearranges predictions correctly.
    """
    preds = numpy.array([[1, 2, 3, 4], [5, 6, 7, 8]])

    spreds = sync_preds(preds, 1)
    assert numpy.allclose(
        spreds, numpy.array([[0, 0, 1, 1], [1, 2, 3, 4], [5, 6, 7, 8]]))


def test_sync_nlls():
    """Tests that sync_nlls rearranges nlls correctly.
    """
    nlls = numpy.array([[1, 2], [3, 4]])

    snlls = sync_nlls(nlls, 2)
    # nans cannot be compared, so we check them separately
    nans = numpy.isnan(snlls)
    indices = numpy.logical_not(nans)
    assert numpy.allclose(
        snlls[indices],
        numpy.array([[numpy.nan, numpy.nan], [numpy.nan, numpy.nan], [1, 2],
                     [3, 4]])[indices])


def test_add_nans():
    numpy.random.seed(10)
    array = numpy.arange(12, dtype=numpy.float64).reshape(4, 3)
    exarray = numpy.array([
        [0, 1, 2],
        [numpy.nan, numpy.nan, numpy.nan],
        [6, 7, 8],
        [9, 10, 11],
    ])
    narray = add_nans(array, 0.5)
    assert numpy.allclose(narray, exarray, equal_nan=True)
