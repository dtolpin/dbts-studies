"""Utilities for learning from monitor time series.

Based on template from
  https://github.com/dtolpin/python-project-skeleton
"""

from os import path

from setuptools import find_packages, setup

import dbts

# Get the long description from the README file
here = path.abspath(path.dirname(__file__))
with open(path.join(here, "README.md")) as f:
    long_description = f.read()


setup(
    name="dbts",
    version=dbts.__version__,

    description="Learning from dbts",
    long_description=long_description,


    packages=find_packages(exclude=["doc", "data"]),

    # source code layout
    namespace_packages=["intensix"],

    # Generating the command-line tool
    entry_points={
        "console_scripts": [
            "prepare=dbts.prepare:main",
            "train=dbts.train:main",
            "predict=dbts.predict:main",
        ]
    },

    # author and license
    author="David Tolpin",
    author_email="david@intensix.com",
    license="Proprietary",

    # dependencies, a list of rules
    # Things assumed to be give (e.g. through anaconda):
    # * numpy
    # * scipy
    # * pandas
    install_requires=["PyYAML>=3.12", "pandas", "torch"],
    # developer dependencies , a list of deps for development environment
    extras_require={"dev": ["pytest", "yapf", "pylint", "isort", "ipython"]},
    # add links to repositories if modules are not on pypi
    dependency_links=[
    ],

    #  PyTest integration
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)
